class CreateClinAbouts < ActiveRecord::Migration[5.2]
  def change
    create_table :clin_abouts do |t|
      t.string :title
      t.string :sub_title
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
