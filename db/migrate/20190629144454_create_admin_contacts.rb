class CreateAdminContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_contacts do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :message

      t.timestamps
    end
  end
end
