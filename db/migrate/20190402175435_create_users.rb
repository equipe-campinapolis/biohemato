class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone
      t.string :cpf
      t.integer :tipo
      t.integer :status
      t.text :annotation

      t.timestamps
    end
  end
end
