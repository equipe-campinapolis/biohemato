class Admin::ContactsController < ApplicationController
  before_action :set_admin_contact, only: [:show, :edit, :update, :destroy]

  def index
    @admin_contacts = Admin::Contact.all
  end

  def show
  end

  def new
    @admin_contact = Admin::Contact.new
  end

  # def edit
  # end

  def create
    @admin_contact = Admin::Contact.new(admin_contact_params)
    respond_to do |format|
      if @admin_contact.save
        format.html { redirect_to @admin_contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: root_path }
      else
        format.html { render :new }
        format.json { render json: @admin_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # def update
  #   respond_to do |format|
  #     if @admin_contact.update(admin_contact_params)
  #       format.html { redirect_to @admin_contact, notice: 'Contact was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @admin_contact }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @admin_contact.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def destroy
    @admin_contact.destroy
    respond_to do |format|
      format.html { redirect_to admin_contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_contact
      @admin_contact = Admin::Contact.find(params[:id])
    end

    def admin_contact_params
      params.require(:admin_contact).permit(:name, :email, :phone, :message)
    end
end
