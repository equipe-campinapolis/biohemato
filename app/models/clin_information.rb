class ClinInformation < ApplicationRecord
  has_one_attached :image #virtual não precisa estar no banco
  validates :title, presence: true,:allow_nil => false
	validates :description, presence: true,:allow_nil => false
	validates :image, presence: true,:allow_nil => false
end
