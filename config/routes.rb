Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/site', as: 'rails_admin'
  root "inicio#index"
  devise_for :users
  namespace :admin do
    resources :contacts
  end
  #get 'doctor/eventdoctor'
  #get 'contact/contactus'
  #get 'service/detailevent'
  #get 'consult/event'
  #get 'team/detail'
  #get 'clinic/index4'
  #get 'about/index3'
  #get 'resultado/resultadoindex'
  get 'inicio/index'
  get 'agenda/index'
  get 'consultas/index'
  get 'equipe/index'
  get 'exames/index'
  get 'contato/index'
  get 'agenda/:id', to: "agenda#index"
end
